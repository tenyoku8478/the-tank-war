TANK WAR 
-
B02902006 陳璽安 B02902062 林天翼 B02902034 邱筱晴  

- What is TANK WAR?  
 ![](http://i.imgur.com/0jNybNT.jpg)

- Our TANK WAR!  
 ![](http://i.imgur.com/WYJYR9Y.png)

- Design the objects:  
player's tank, enemy's tank, soft/hard bricks, bullets, and QB  

- Tools  
[Painter](http://homepage.ntu.edu.tw/~b02902062/painter.html) and [Map Designer](http://homepage.ntu.edu.tw/~b02902062/map.html)  

- Classes   
Main, Game, Menu, Album, Image, Scalable, Book, Map, Statement, Label, Tank, Bullet, AI, Player, Bomb, List, Node, Rand  

- Implement Class List:  
    ```
    class List{  
        method void reset()   
      method boolean end()  
        method boolean next()  
        method int get()  
        method void insert(int data)  
        method void delete()  
    }    
    ```

- Implement random function:  
    ```
    function int rand() {  
        let a = a*a+t/16&127;  
        let t = t+1;  
        return a;  
    }  
    ```

- Statistic:
    - 20 Classes 
    - 45 Functions
    - 45 Methods
    - 13 Constructors
    - Code (with & without Album) :   2853 / 1364 lines
- Check out the presentation ppt [here](https://drive.google.com/file/d/0B1I8--mbkMQ8b3NlcXVIS0VFM0E/view?usp=sharing)
